<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Post extends Model
{
    use HasFactory;
    protected $table = 'post';

    public function tag()
    {
        return $this->belongsTo(Tag::class , 'tag_id', 'id');
    }

    public function subtag()
    {
        return $this->belongsTo(SubTag::class , 'subtag_id', 'id');
    }

    protected $fillable = [
        'id',
        'title',
        'picture',
        'content',
        'tag_id',
        'subtag_id'
    ];
}

?>
