<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Tag extends Model
{
    use HasFactory;
    protected $table = 'tag';

    // public function post() {
    //     return $this->hasMany(Post::class, 'post_id', 'id');
    // }

    public function subtag() {
        return $this->hasMany(SubTag::class);
    }

    protected $fillable = [
        'id',
        'name',
        'subtag_id',
        'post_id'
      ];
}

?>
