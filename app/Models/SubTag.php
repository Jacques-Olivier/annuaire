<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class SubTag extends Model
{
    use HasFactory;
    protected $table = 'subtag';

    public function tag () {
        return $this->belongsTo(Tag::class, 'tag_id', 'id');
    }

    // public function post() {
    //     return $this->hasMany(Post::class, 'post_id', 'id');
    // }

    protected $fillable = [
        'id',
        'tag_id',
        'post_id',
        'name'
      ];
}

?>
