<?php

namespace App\Http\Controllers;

use App\Models\Tag;
use App\Models\SubTag;
use Illuminate\Http\Request;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tags = Tag::all();
        $modification = false;
        $creation = false;
        return view('/tags/tag_dashboard', compact('tags', 'modification', 'creation'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        $tags = Tag::all();
        $modification = false;
        $creation = true;
        return view('/tags/tag_dashboard', compact('tags', 'creation', 'modification'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'bail|required|string|max:50',
        ];

        $this->validate($request, $rules);

        $tag = new Tag();
        $tag->name = $request->name;
        Tag::create([
            'name' => $request->name,
            'subtag_id' => null
        ]);
        $creation = false;
        $modification = false;
        // 4. On retourne vers tous les posts : route("posts.index")
        return redirect(route("tag_dashboard", compact('modification')));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tag = Tag::findOrFail($id);
        $subtags = SubTag::all()->where('tag_id', '=', $id);
        $modification = false;
        $creation = false;
        return view('/tags/show_tag', compact('tag', 'subtags', 'creation', 'modification'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tagToEdit = Tag::findOrFail($id);
        $tags = Tag::all();
        $modification = true;
        $creation = false;
        return view('/tags/tag_dashboard', compact('tags', 'tagToEdit', 'modification', 'creation'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tag $tag)
    {
        $rules = [
            'name' => 'bail|required|string|max:50',
        ];

        $this->validate($request, $rules);

        Tag::where('id', $request->id)->update([
            'name' => $request->name
        ]);
        return redirect(route("tag_dashboard"));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        Tag::where('id', $id)->firstOrFail()->delete();
        return redirect(route("tag_dashboard"));
    }
}
