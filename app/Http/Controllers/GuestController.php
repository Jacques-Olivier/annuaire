<?php

namespace App\Http\Controllers;

use App\Models\Tag;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GuestController extends Controller
{
    public function index(Request $request)
    {
        $posts = $this->filter($request);
        $tags = Tag::all();
        return view('guests/guest_dashboard', compact('posts', 'tags'));
    }

    public function filter(Request $request)
    {
        $tag_id = $request['filter'];
        if($tag_id == null) {
            $posts = Post::all();
        } else {
            $posts = Post::when($tag_id, function($query, $tag_id) {
                $query->where('tag_id', $tag_id);
            })->get();
        }
        return $posts;
    }

    public function show($id)
    {
        // $post = Post::with('tag')->where('id', $id)->get();
        $post = Post::findOrFail($id);
        return view('/guests/show', compact('post'));
    }
}
