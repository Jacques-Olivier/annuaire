<?php

namespace App\Http\Controllers;

use App\Models\SubTag;
use App\Models\Tag;
use Illuminate\Http\Request;

class SubTagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subtags = SubTag::all();
        $modification = false;
        $creation = false;
        return view('tags/subtag_dashboard', compact('subtags', 'modification', 'creation'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create($id)
    {
        $tag = Tag::findOrFail($id);
        $subtags = SubTag::all()->where('tag_id', '=', $id);
        $modification = false;
        $creation = true;
        // return view('subtag.dashboard', compact('subtags', 'creation', 'modification'));
        return view('/tags/show_tag', compact('tag', 'subtags', 'creation', 'modification'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'bail|required|string|max:50',
        ];

        $this->validate($request, $rules);

        // $subtag = new SubTag();
        // $subtag->subtag_name = $request->subtag_name;
        // $subtag->tag_id = $request->tag_id;
        // dd($request->tag_id);
        SubTag::create([
            'name' => $request->name,
            'tag_id' => $request->tag_id
        ]);
        return redirect(route('tags.show', ['id' => $request['tag_id']]));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SubTag  $subtag
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $tag = Tag::findOrFail($id);
        // $sub_tags = SubTag::all()->where('tag_id', '=', $id);
        // return view('show_tag', compact('tag', 'sub_tags'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SubTag  $subtag
     * @return \Illuminate\Http\Response
     */
    public function edit($id, $tag_id)
    {
        $tag = Tag::findOrFail($tag_id);
        $subtagToEdit = SubTag::findOrFail($id);
        $subtags = SubTag::all()->where('tag_id', '=', $tag_id);
        $modification = true;
        $creation = false;
        return view('/tags/show_tag', compact('tag', 'subtags', 'subtagToEdit', 'modification', 'creation'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SubTag  $subtag
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $rules = [
            'name' => 'bail|required|string|max:50',
        ];

        $this->validate($request, $rules);

        SubTag::where('id', $request->id)->update([
            'name' => $request->name
        ]);
        return redirect(route('tags.show', ['id' => $request['tag_id']]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SubTag  $subtag
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $tag_id)
    {
        SubTag::where('id', $id)->firstOrFail()->delete();
        return redirect(route('tags.show', ['id' => $tag_id]));
    }
}
