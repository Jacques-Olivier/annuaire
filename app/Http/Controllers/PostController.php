<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\Tag;
use App\Models\SubTag;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $posts = $this->filter($request);
        $tags = Tag::all();
        return view('dashboard', compact('posts', 'tags'));
    }

    public function filter(Request $request)
    {
        $tag_id = $request['filter'];
        if($tag_id == null) {
            $posts = Post::all();
        } else {
            $posts = Post::when($tag_id, function($query, $tag_id) {
                $query->where('tag_id', $tag_id);
            })->get();
        }
        return $posts;
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $tags = Tag::all();
        $subtags = SubTag::all();
        return view('create', compact('tags', 'subtags'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        $rules = [
            'title' => 'bail|required|string|max:255',
            "picture" => 'bail|required|image|max:1024',
            "content" => 'bail|required',
            "tag_id" => 'bail|required|exists:App\Models\Tag,id',
            "subtag_id" => 'bail|exists:App\Models\SubTag,id'
        ];
        $this->validate($request, $rules);

        $post = new Post();
        $post->title = $request->title;
        $post->picture = $request->picture;
        $post->content = $request->content;
        $post->subtag_id = (int) $request->subtag_id;
        $post->tag_id = (int) $request->tag_id;

        $chemin_image = $request->picture->store("/");

        Post::create([
            'title' => $request->title,
            'picture' => $chemin_image,
            'content' => $request->content,
            'tag_id' => $request->tag_id,
            'subtag_id' => $request->subtag_id
        ]);
        return redirect(route("dashboard"));
    }

    /**
     * Display the specified resource.
     * @param  \App\Models\Post  $post
     */
    public function show($id)
    {
        $post = Post::findOrFail($id);
        return view('show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param  \App\Models\Post  $post
     */
    public function edit($id)
    {
        $post = Post::findOrFail($id);
        return view('edit', compact([
            'post']));
    }

    /**
     * Update the specified resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     */
    public function update(Request $request)
    {
        $rulesIfOldPicture = [
            'title' => 'bail|required|string|max:255',
            "content" => 'bail|required'
        ];
        $chemin_image = $request->picture;

        if($chemin_image == null) {
            $this->validate($request, $rulesIfOldPicture);
            Post::where('id', $request->id)->update([
                'title' => $request->title,
                'content' => $request->content
            ]);
        } else {
            $rulesIfOldPicture = Arr::add($rulesIfOldPicture,
            'picture', 'bail|required|image|max:1024');

            $this->validate($request, $rulesIfOldPicture);
            $chemin_image = $request->picture->store("/");
            Post::where('id', $request->id)->update([
                'title' => $request->title,
                'picture' => $chemin_image,
                'content' => $request->content
            ]);
        }
        return redirect(route("dashboard"));
    }

    /**
     * Remove the specified resource from storage.
     * @param  \App\Models\Post  $post
     */
    public function destroy(string $id)
    {
        Post::where('id', $id)->firstOrFail()->delete();
        return redirect(route("dashboard"));
    }
}
