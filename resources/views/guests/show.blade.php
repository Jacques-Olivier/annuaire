<x-guest-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ $post['title'] }}
        </h2>
    </x-slot>

    <div class="flex flex-row gap-4 m-4 justify-center">
        <div class="w-fit h-fit max-w-[30%] max-h-[30%] border-2 border-gray-500">
            <img src="{{ asset('storage/' . $post['picture']) }}" alt="Image de couverture" class="">
        </div>

        <div class="flex-column justify-between flex flex-col mb-6 border rounded-lg p-4 bg-white flex flex-col gap-6 border-2 border-slate-600">
            <div>{{ $post['content'] }}</div><br>
            <div class="flex flex-row gap-4 justify-end">
                <div class="w-fit p-2 bg-red-800 text-white rounded-full self-end font-ubuntu antialiased">{{ $post->tag->name }}</div>
                <div class="w-fit p-2 bg-red-800 text-white rounded-full self-end font-ubuntu antialiased">{{ $post->subtag->name }}</div>
            </div>
        </div>
    </div>

    <div class="flex justify-center">
        <a href="{{ route('guest.dashboard') }}" title="Retourner aux articles"
            class="bg-gray-300 hover:bg-gray-200 active:border-gray-100 border-b-2 border-gray-700 rounded-full py-2 px-4">Retourner
            aux posts</a>
    </div>

</x-guest-layout>

