<x-guest-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Articles') }}
        </h2>
    </x-slot>

    <div class="flex items-center flex-col">
        <div class="flex flex-row">
            <form method="GET" action="{{ route('guest.dashboard') }}" enctype="multipart/form-data">
                @csrf
                <select name="filter" id="filtre">
                    <option value="">Sélectionner un filtre</option>
                    @foreach ($tags as $tag)
                        <option value="{{ $tag['id'] }}">{{ $tag['name'] }}</option>
                    @endforeach
                </select>
                @error('filter')
                    <div>{{ $message }}</div>
                @enderror
                <button type="submit"
                    class="m-6 bg-gray-200 border-b-2 border-gray-700 rounded-full py-2 px-4">Filtrer</button>
            </form>
        </div>

        @if ($posts)
            <div class="flex flex-row flex-wrap justify-start mt-8 gap-8 w-[90%]">
                @foreach ($posts as $post)
                    {{-- <div class="event_date"></div> --}}
                    <div
                        class="event w-80 h-72 rounded-lg p-4 bg-white flex flex-col place-content-between	border-2 border-slate-600">
                        <div class="w-fit bg-red-800 text-white rounded-full self-end font-ubuntu antialiased">
                            <span class="py-0.5 px-2">{{ $post->tag->name }}</span>
                        </div>
                        <div>
                            <a href="{{ route('guest.posts.show', ['id' => $post->id]) }}" title="Lire l'article"
                                class="w-40 title break-words font-ubuntu text-2xl">{{ $post->title }}</a>
                        </div>
                        {{-- <p class="event_time"></p> --}}
                        {{-- <p class="event_location"></p> --}}
                    </div>
                @endforeach
            </div>
        @endif
    </div>
</x-guest-layout>
