<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Sous-Tags') }}
        </h2>
    </x-slot>

    <div class="h-auto flex items-center flex-col mt-4">
        @if ($subtags)
            <table class="table bg-white w-fit tab border-solid border-black border-2" border-collapse="collapse">
                <thead class="text-3xl border-solid border-black border-2">
                    <tr class="bg-gray-300">
                        <th class="border-solid border-black border-2 w-fit p-4">Nom du sous-tag</th>
                        <th class="border-solid border-black border-2 w-fit p-4" colspan="2">Nom du tag</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($subtags as $subtag)
                        <tr class="text-2xl even:bg-gray-300">
                            <td class="p-3 text-center border-solid border-black border-2 hover:underline">
                                <span title="Nom du sous-tag">{{ $subtag['name'] }}</span>
                            </td>
                            <td class="p-3 text-center border-solid border-black border-2 hover:underline">
                                <span title="Nom du tag">{{ $subtag->tag->name }}</span>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @else
            <span>Aucun sous-tag actuellement.</span>
        @endif

        @if ($modification)
            @include('edit_subtag')
        @endif

    </div>
</x-app-layout>
