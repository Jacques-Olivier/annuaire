    <!-- Le formulaire est géré par la route "posts.store" -->
    <form method="POST" action="{{ route('subtags.store') }}" enctype="multipart/form-data"
        class="flex flex-col mb-6 mt-2 border rounded-lg p-4 bg-white flex flex-col gap-6 border-2 border-slate-600">

        <!-- Le token CSRF -->
        @csrf
        <input type="hidden" name="tag_id" value={{ $tag['id'] }} id="tag_id" placeholder="">

        <div>
            <label for="name">Nom du sous-tag</label><br />
            <input type="text" name="name" value="{{ old('name') }}" id="name"
                placeholder="Le nom du sous-tag">

            <!-- Le message d'erreur pour "title" -->
            @error('name')
                <div>{{ $message }}</div>
            @enderror

            <div class="flex flex-row justify-between mt-4 ">
                <button type="submit"
                    class="self-center text-white bg-red-700 hover:bg-red-600 active:border-white font-bold py-2 px-4 border-b-2 border-slate-800 hover:border-slate-700 rounded-full">Créer</button>
                <a href="{{ route('tags.show', ['id' => $tag['id']]) }}" title="Annuler"
                    class="bg-gray-300 hover:bg-gray-200
                                        active:border-gray-100 border-b-2 border-gray-700 rounded-full py-2 px-4"">Annuler</a>
            </div>
        </div>
    </form>
