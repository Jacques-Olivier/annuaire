<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ $tag['name'] }}
        </h2>
    </x-slot>

    <div class="flex items-center flex-col">
        <div class="flex self-center">
            <!-- Lien pour créer un nouvel article : "create" -->
            <a href="{{ route('subtags.create', ['tagId' => $tag['id']]) }}" title="Créer un sous-tag" class="my-4 self-center text-white bg-red-700 hover:bg-red-600 active:border-white font-bold py-2 px-4 border-b-2 border-slate-800 hover:border-slate-700 rounded-full">Créer un nouveau
                sous-tag</a>
        </div>

        @if ($creation)
            @include('tags/create_subtag')
        @endif

        @if ($modification)
            @include('tags/edit_subtag')
        @endif

        @if ($subtags->isEmpty())
            <span class="mb-6">Aucun tag actuellement.</span>
        @else
            <table class="table bg-white w-fit tab border-solid border-black border-2 mb-6 border-collapse="collapse">
                <thead class="text-3xl border-solid border-black border-2">
                    <tr class="bg-gray-300">
                        <th class="border-solid border-black border-2 w-fit p-4">Nom du sous-tag</th>
                        <th class="border-solid border-black border-2 w-fit p-4" colspan="2">Opérations</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($subtags as $sub)
                        <tr class="text-2xl even:bg-gray-300">
                            <td class="border-solid border-black border-2 text-center">
                                <span>{{ $sub['name'] }}</span>
                            </td>
                            <td class="p-3 border-solid border-black border-2 hover:underline">
                                <a href="{{ route('subtags.edit', ['id' => $sub['id'], 'tag_id' => $tag['id']]) }}"
                                    title="Modifier le tag">Modifier</a>
                            </td>
                            <td class="p-3 border-solid border-black border-2 hover:underline">
                                <a href="{{ route('subtags.destroy', ['id' => $sub['id'], 'tag_id' => $tag['id']]) }}">Supprimer</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @endif
        <div>
            <a href="{{ route('tag_dashboard') }}" title="Retourner aux tags" class="m-6 bg-gray-300 hover:bg-gray-200 active:border-gray-100 border-b-2 border-gray-700 rounded-full py-2 px-4">Retourner aux tags</a>
        </div>
    </div>
</x-app-layout>
