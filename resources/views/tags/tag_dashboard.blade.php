<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Tags') }}
        </h2>
    </x-slot>

    <div class="flex items-center flex-col">
        <div class="flex self-center"><a href="{{ route('tags.create') }}" title="Créer un article"
                class="text-white my-4 bg-red-700 hover:bg-red-600 font-bold py-2 px-4 border-b-2 border-slate-800 hover:border-slate-700 rounded-full">Créer
                un nouveau tag</a>
        </div>
        {{-- @if ($tags)
            <table class="tab bg-slate-500 border-4 border-solid border-red-500">
                <thead class="border-4 border-solid border-red-500">
                    <tr>
                        <th class="border-4 border-solid border-red-500">Nom du tag</th>
                        <th colspan="2">Opérations</th>
                    </tr>
                </thead>
                <tbody>
                    <!-- On parcourt la collection de Post -->
                    @foreach ($tags as $tag)
                        <tr class="">
                            <td class="">
                                <a href="{{ route('tags.show', ['id' => $tag['id']]) }}"
                                    title="Voir le tag">{{ $tag['name'] }}</a>
                            </td>
                            <td class="">
                                <a href="{{ route('tags.edit', ['id' => $tag['id']]) }}" title="Modifier le tag"
                                    class="bg-blue-500 hover:bg-blue-400 text-black font-bold py-2 px-4 border-b-4 border-blue-700 hover:border-blue-500 rounded">Modifier</a>
                            </td>
                            <td>
                                <a href="{{ route('tags.destroy', $tag['id']) }}">Supprimer</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table> --}}
        {{-- @else
            <span>Aucun tag actuellement.</span>
        @endif --}}

        @if ($modification)
            @include('tags/edit_tag')
        @endif

        @if ($creation)
            @include('tags/create_tag')
        @endif

    </div>
    @if ($tags)
        <div class="h-auto flex items-center flex-col">
            <table class="table bg-white w-fit tab border-solid border-black border-2" border-collapse="collapse">
                <thead class="text-3xl border-solid border-black border-2">
                    <tr class="bg-gray-300">
                        <th class="border-solid border-black border-2 w-fit p-4">Nom du tag</th>
                        <th class="border-solid border-black border-2 w-fit p-4" colspan="2">Opérations</th>
                    </tr>
                </thead>
                <tbody>
                    <!-- On parcourt la collection de Post -->
                    @foreach ($tags as $tag)
                        <tr class="text-2xl even:bg-gray-300">
                            <td class="border-solid border-black border-2 text-center hover:underline">
                                <a href="{{ route('tags.show', ['id' => $tag['id']]) }}"
                                    title="Voir le tag">{{ $tag['name'] }}</a>
                            </td>
                            <td class="p-3 border-solid border-black border-2 hover:underline">
                                <a href="{{ route('tags.edit', ['id' => $tag['id']]) }}"
                                    title="Modifier le tag">Modifier</a>
                            </td>
                            <td class="p-3 border-solid border-black border-2 hover:underline">
                                <a href="{{ route('tags.destroy', $tag['id']) }}">Supprimer</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
    @endif
    </div>
</x-app-layout>
