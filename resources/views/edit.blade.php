<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Modifier') }}
        </h2>
    </x-slot>

    <div class="flex items-center flex-col mt-6">
        <!-- Le formulaire est géré par la route "posts.store" -->
        <form method="POST" action="{{ route('posts.update') }}" enctype="multipart/form-data" class="flex flex-col border rounded-lg p-4 bg-white flex flex-col gap-6	border-2 border-slate-600">

            <!-- Le token CSRF -->
            @csrf

            <div>
                <input type="hidden" name="id" value="{{ $post->id }}">
                <label for="title" class="font-bold">Titre</label><br/>
                <input type="text" name="title" value="{{ $post['title'] }}"  id="title" placeholder="Le titre du post" class="w-[100%]">

                <!-- Le message d'erreur pour "title" -->
                @error("title")
                <div>{{ $message }}</div>
                @enderror
            </div>

            <div>
                <span class="font-bold">Couverture actuelle</span><br/>
                <div class="w-fit h-fit border-2 border-gray-500">
                    <img src="{{ asset('storage/' . $post['picture']) }}" alt="image de couverture actuelle" style="max-height: 200px;" >
                </div>
            </div>
            <div>
                <label for="picture" class="font-bold">Modifier la couverture</label><br/>
                <input type="file" name="picture" id="picture" alt="Test">

                <!-- Le message d'erreur pour "picture" -->
                @error("picture")
                <div>{{ $message }}</div>
                @enderror
            </div>

            <div>
                <label for="content" class="font-bold">Contenu</label><br/>
                <textarea name="content" id="content" lang="fr" rows="10" cols="50" placeholder="Le contenu du post" >{{ $post['content'] }}</textarea>

                <!-- Le message d'erreur pour "content" -->
                @error("content")
                <div>{{ $message }}</div>
                @enderror
            </div>

            <button type="submit" class="mb-3 self-center text-white bg-red-700 hover:bg-red-600 active:border-white font-bold py-2 px-4 border-b-2 border-slate-800 hover:border-slate-700 rounded-full">Modifier</button>

        </form>

        <div class="flex justify-center">
            <a href="{{ route('dashboard') }}" title="Retourner aux articles"
                class="my-4 bg-gray-300 hover:bg-gray-200 active:border-gray-100 border-b-2 border-gray-700 rounded-full py-2 px-4">Retourner
                aux posts</a>
        </div>

    </div>

</x-app-layout>
