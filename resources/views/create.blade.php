<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Nouvel article') }}
        </h2>
    </x-slot>

    <!-- Le formulaire est géré par la route "posts.store" -->
    <div class="flex items-center flex-col mt-6">
        <form class="flex flex-col border rounded-lg p-4 bg-white flex flex-col gap-6	border-2 border-slate-600"
            method="POST" action="{{ route('posts.store') }}" enctype="multipart/form-data">

            @csrf
            <div>
                <label for="title" class="font-bold">Titre</label><br />
                <input class="w-[100%]" type="text" name="title" value="{{ old('title') }}" id="title"
                    placeholder="Le titre du post">
                @error('title')
                    <div class="text-red-500">Le titre est obligatoire.</div>
                @enderror
            </div>

            <div>
                <label for="picture" class="font-bold">Couverture</label><br />
                <input type="file" name="picture" id="picture">
                @error('picture')
                    <div class="text-red-500">L'image de couverture est obligatoire.</div>
                @enderror
            </div>

            <div>
                <label for="content" class="font-bold">Contenu</label><br />
                <textarea name="content" id="content" lang="fr" rows="10" cols="50" placeholder="Le contenu du post"></textarea>
                @error('content')
                    <div>{{ $message }}</div>
                @enderror
            </div>

            <div>
                <label for="tag_id" class="font-bold">Tags</label><br />
                <select name="tag_id" id="tag_id" class="w-[50%]">
                    <option value="">Choisir un tag</option>
                    @foreach ($tags as $tag)
                        <option value="{{ $tag['id'] }}">{{ $tag['name'] }}</option>
                    @endforeach
                </select>
                @error('tag_id')
                    <div>{{ $message }}</div>
                @enderror
            </div>

            <div>
                <label for="subtag_id" class="font-bold">Sous-tags</label><br />
                <select name="subtag_id" id="subtag_id" class="w-[50%]">
                    <option value="">Choisir un sous-tag</option>
                    @foreach ($subtags as $subtag)
                        <option value="{{ $subtag['id'] }}">{{ $subtag['name'] }}</option>
                    @endforeach
                </select>
                @error('subtags')
                    <div>{{ $message }}</div>
                @enderror
            </div>

            <button type="submit" class="self-center text-white bg-red-700 hover:bg-red-600 active:border-white
            font-bold py-2 px-4 border-b-2 border-slate-800 hover:border-slate-700 rounded-full">Créer</button>

        </form>
        <a href="{{ route('dashboard') }}" title="Retourner aux articles"
            class="m-6 bg-gray-300 hover:bg-gray-200 active:border-gray-100 border-b-2 border-gray-700 rounded-full py-2 px-4">
            Retourner aux posts</a>
    </div>

</x-app-layout>
