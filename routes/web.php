<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TagController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\GuestController;
use App\Http\Controllers\SubTagController;

/* |-------------------------------------------------------------------------- | Web Routes |-------------------------------------------------------------------------- | | Here is where you can register web routes for your application. These | routes are loaded by the RouteServiceProvider within a group which | contains the "web" middleware group. Now create something great! | */

Route::get('/', [GuestController::class , 'index'])->name('guest.dashboard');
Route::get('/show/{id}', [GuestController::class , 'show'])->name('guest.posts.show');

Route::get('/dashboard', [PostController::class , 'index'])->middleware(['auth'])->name('dashboard');
Route::get('/posts/create', [PostController::class , 'create'])->name('posts.create')->middleware(['auth']);
Route::post('/posts/create', [PostController::class , 'store'])->name('posts.store')->middleware(['auth']);
Route::post('/posts/update', [PostController::class , 'update'])->name('posts.update')->middleware(['auth']);
Route::get('/posts/destroy/{post}', [PostController::class , 'destroy'])->name('posts.destroy')->middleware(['auth']);
Route::get('/posts/edit/{id}', [PostController::class , 'edit'])->name('posts.edit')->middleware(['auth']);
Route::get('/posts/{id}', [PostController::class , 'show'])->name('posts.show')->middleware(['auth']);

Route::get('/tags', [TagController::class , 'index'])->name('tag_dashboard')->middleware(['auth']);
Route::get('/tags/create', [TagController::class , 'create'])->name('tags.create')->middleware(['auth']);
Route::post('/tags/create', [TagController::class , 'store'])->name('tags.store')->middleware(['auth']);
Route::post('/tags/update', [TagController::class , 'update'])->name('tags.update')->middleware(['auth']);
Route::get('/tags/destroy/{tag}', [TagController::class , 'destroy'])->name('tags.destroy')->middleware(['auth']);
Route::get('/tags/edit/{id}', [TagController::class , 'edit'])->name('tags.edit')->middleware(['auth']);
Route::get('/tags/{id}', [TagController::class , 'show'])->name('tags.show')->middleware(['auth']);

Route::get('/subtags', [SubTagController::class , 'index'])->name('subtags.dashboard')->middleware(['auth']);
Route::get('/subtags/create/{tagId}', [SubTagController::class , 'create'])->name('subtags.create')->middleware(['auth']);
Route::post('/subtags/create', [SubTagController::class , 'store'])->name('subtags.store')->middleware(['auth']);
Route::post('/subtags/update', [SubTagController::class , 'update'])->name('subtags.update')->middleware(['auth']);
Route::get('/subtags/destroy/{id}/{tag_id}', [SubTagController::class , 'destroy'])->name('subtags.destroy')->middleware(['auth']);
Route::get('/subtags/edit/{id}/{tag_id}', [SubTagController::class , 'edit'])->name('subtags.edit')->middleware(['auth']);

require __DIR__ . '/auth.php';
