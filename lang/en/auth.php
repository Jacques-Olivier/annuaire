<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Les identifiants de connexion ne correspondent pas.',
    'password' => 'Le mot de passe entré est incorrect.',
    'throttle' => 'Trop d\'essais de connexion. Merci de réessayer dans :seconds secondes.',

];
