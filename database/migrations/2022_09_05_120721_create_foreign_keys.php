<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tag', function (Blueprint $table) {
            $table->foreign('subtag_id')->references('id')->on('subtag');
        });

        Schema::table('subtag', function (Blueprint $table) {
            $table->foreign('tag_id')->references('id')->on('tag');
        });

        Schema::table('post', function (Blueprint $table) {
            $table->foreign('tag_id')->references('id')->on('tag');
            $table->foreign('subtag_id')->references('id')->on('subtag');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('foreign_keys');
    }
};
